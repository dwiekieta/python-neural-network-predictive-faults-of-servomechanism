import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
import time
import pyodbc

# 1. Wczytanie bazy probek dataSet
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
dataHistory = 100
fileName = fileDirectory + "\\NEW100finalDataSet.csv"
data = pd.read_csv(fileName)

# 2. Rozbicie danych na wejscie i wyjscie
#X,y = data.iloc[:,(int((len(data.values[0,:])-1)/2)):len(data.values[0,:])-1].values, \
#
#X,y = data.iloc[:,:int((len(data.values[0,:])-1)/2)].values, \
X,y = data.iloc[:,:int((len(data.values[0,:])-1))].values, \
    data.iloc[:,len(data.values[0,:])-1].values

# 3. Utworzenie zbiorow uczacych oraz walidacyjnych w proporcji 70/30
X_uczacy,X_walidacyjny,y_uczacy,y_walidacyjny = \
    train_test_split(X, y, test_size=0.3, random_state=0)

# 4. Wyuczenie klasyfikatora RBF
# 4.1 Rodzaj klasyfikatora
klasyfikator = SVC(kernel='rbf', random_state=0, 
                   gamma=0.1,C=100)
# 4.2 Wyuczdnie klasyfikatora
klasyfikator.fit(X_uczacy,y_uczacy)
# 4.3 Wynik predykcji
score = klasyfikator.score(X_walidacyjny,y_walidacyjny)  
print("Score: " + str(score))

# 5. Baza danych
# 5.1 Dane logowania
Driver = "{ODBC Driver 13 for SQL Server}"
Server="tcp:projekt-mitsubishi.database.windows.net"
Database= "MitsubishiServo"
Uid= "dkpp@projekt-mitsubishi"
Pwd= "Haslo1234"
Encrypt= "yes"
TrustServerCertificate = "no"
ConnectionTimeout = 30
# 5.2 Ustanowienie polaczenia
cnxn = pyodbc.connect('DRIVER='+Driver+';SERVER='+Server+';PORT=1443;DATABASE='+Database+';UID='+Uid+';PWD='+Pwd)
try:
    cursor = cnxn.cursor()
except e:
    print("Błąd połączenia z bazą danych")

while True:
    # 5.3 Wybranie tabeli z danynmi
    top = 1000
    cursor.execute("SELECT top " + str(top) + " servo_N, servo_realSpeed, servo_Torque from daneServo order by ID desc")
    row = cursor.fetchone()
    # 5.4 Odczyt danych
    # 5.4.1 Macierz wejsciowa
    dataInput = np.ones(2*dataHistory)
    i = 0     
    predict = 0;
    rows = int(top/dataHistory)
    for r in range(rows):
        for i in range(dataHistory):
            if row[1] > 0:
                predkosc = row[1]-row[0]
            else:
                predkosc = row[1]+row[0]

            if row[0] == 0:
                predkosc /= 99999999
            else:
                predkosc /= row[0]
            dataInput[i]= (predkosc+1)/2
            dataInput[i+dataHistory] = (row[2]+387)/(426+387)
            row = cursor.fetchone()
        # 5.5 Predykcja wyniku
        predict += klasyfikator.predict([dataInput])[0]  
        #print(dataInput)
        #print("\n")
    predict /= float(rows)
    predict *= 100
    #predict = klasyfikator.predict([X_uczacy[40]])
    print(int(predict))
    # 5.6 Zapisanie wyniku
    cursor.execute("insert into predictServo values (" + str(predict) + ")")
    cnxn.commit()
    time.sleep(1.5)