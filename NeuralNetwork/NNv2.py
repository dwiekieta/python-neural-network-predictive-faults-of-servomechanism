import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

# 1. Lokalizacja plikow na dysku
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset\\"
fileDataSets = ["10", "20", "40", "60", "80", "100"]
fileScores = ["bezPrzetrenowania", "bezPrzetrenowania95", "wszystkieWyniki"]
filePrefix = "STD"
fileSuffix = "finalDataSet.csv"
# 1.1 Otwarcie plików wynikowych
bP = open(fileDirectory+fileScores[0]+".csv", "w")
bPp = open(fileDirectory+fileScores[1]+".csv", "w")
wW = open(fileDirectory+fileScores[2]+".csv", "w")
# 1.2 Wpisanie pierwszych wierszy do plikow wynikowych
bP.write("Historia,Model,C,gamma,scoreU,scoreW,fit\n")
bPp.write("Historia,Model,C,gamma,scoreU,scoreW,fit\n")
wW.write("Historia,Model,C,gamma,scoreU,scoreW,fit\n")

# 2. Program optymalizacyjny
# 2.1 ProgressVariable
# 2.1.1 Liczba petli wilekosci wektora danych
H = 6
# 2.1.2 Liczba petli rodzajow klasyfikatorow
K = 3
Kstr = ["reg. log.", "SVC", "RBF"]
# 2.1.3 Liczba petli poziomu regularyzacji danych
Cmin = -2
Cmax = 3
C = Cmax-Cmin
# 2.1.4 Liczba petli poziomu odchylenia danych
Gmin = -2
Gmax = 3
G = Gmax-Gmin
# 2.1.5 totalLoops
tL = H*(K*C + G)
# 2.1.5 currentLoop
cL = 0
pL = -1

# 2.2 Petla wielkosci wektora
for h in range(H):
    # 2.2.1 Wczytanie pliku CSV z danymi
    data = pd.read_csv(fileDirectory+filePrefix+fileDataSets[h]+fileSuffix)
    # 2.2.2 Wyluskanie danych z pliku CSV
    X,y = data.iloc[:,:len(data.values[0,:])-1].values, \
    data.iloc[:,len(data.values[0,:])-1].values
    # 2.2.3 Rozbicie danych na uczace oraz walidacyjne 70/30
    X_uczacy,X_walidacyjny,y_uczacy,y_walidacyjny = \
        train_test_split(X,y, test_size=0.3, random_state=0)

    # 2.3 Petla klasyfikatorow
    for k in range(K):
        # 2.4 Petla regularyzacji
        for c in range(Cmin,Cmax):
            # 2.5 Petla odchylenia
            for g in range(Gmin,Gmax):
                # 2.5.1 Wybor kalsyfikatra
                if k == 0:
                    klasyfikator = LogisticRegression(C=10**c,random_state=0)
                elif k == 1:
                    klasyfikator = SVC(kernel='linear',C=10**c,random_state=0)
                else:
                    klasyfikator = SVC(C=10**c,gamma=10**g,random_state=0)

                # 2.5.2 Wyuczenie klasyfikatora
                klasyfikator.fit(X_uczacy,y_uczacy)
                # 2.5.3 Wynik klasyfikacji
                sU = round(klasyfikator.score(X_uczacy,y_uczacy)*100,2)
                sW = round(klasyfikator.score(X_walidacyjny,y_walidacyjny)*100,2)
                sP = round(sW-sU,2)

                cC = round(10**c,-Cmin)
                gG = round(10**g,-Gmin)

                # 2.5.4 Zapis danych do plikow wynikowych
                wW.write(fileDataSets[h] + "," + Kstr[k] + "," + str(cC) + "," + str(gG) + "," + str(sU) + "," + str(sW) + "," + str(sP) + "\n")
                if sP+0.5 > 0:
                    bP.write(fileDataSets[h] + "," + Kstr[k] + "," + str(cC) + "," + str(gG) + "," + str(sU) + "," + str(sW) + "," + str(sP) + "\n")
                    if sW >= 95:
                        bPp.write(fileDataSets[h] + "," + Kstr[k] + "," + str(cC) + "," + str(gG) + "," + str(sU) + "," + str(sW) + "," + str(sP) + "\n")

                # 2.5.5 Aktualizacja postepu
                cL += 1
                curr = round((cL/tL)*100,1)
                if curr != pL:
                    pL = curr
                    print(pL)

                # 2.5.6 Sprawdzenie koniecnzosci zmiany gammy
                if k != 2:
                    break

# 3. Zamkniecie plikow wynikowych
bP.close
bPp.close
wW.close
                    