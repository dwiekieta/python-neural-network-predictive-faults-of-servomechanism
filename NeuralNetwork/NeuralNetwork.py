# NeuralNetwork
# Skrypt wykorzystuje dane zebrane, przeanalizowane oraz przetworzone 
#   przez wczesniejsze skrypty do opracowania modelu probabilistcznego 
#   sieci neuronowej rozwiazujacej problem klasyfikacji rodzaju nastaw 
#   regulatora serwomechanizmu jako 
#       automatyczny badz manualny
#       
#   Skrypt po wczytnaiu danych tworzy zbiory testu krzyzowego 70/30
#   Nastepnie sprawdzane sa rozne konfiguracje klayfikatora dla 
#       trzech rodzajow danych wejsciowych:
#           wszystkich/predkosci/przeciazenia
#       Dla trzech rodzajow klasyfikatorow
#           model regresji logistycznej(liniowy)
#           model maszyny wektorow nosnych SVM(liniowy)
#           model SVM z jadrem gaussowskim RBF(nieliniowy)
#       W każdym modelu zmieniano parametry
#           C - odwrotnosc parametru regulacyjnego(L2) 
#           gamma - parametr jadra radialnej funkcji bazowej 
#   Wykorzystano gotowe rozwiazania z biblioteki sklearn
#   
#   Wynikiem pracy skryptu jest plik z punktem pracy oraz wynikiem bedacym 
#       prawdopodobienstwem z jakim predyktor dokona prawidlowej 
#       klasyfikacji danych wejsciowych 

import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

# 1. Wczytanie bazy probek dataSet
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
fileName = fileDirectory + "\\finalDataSet10.csv"
data10 = pd.read_csv(fileName)
fileName = fileDirectory + "\\finalDataSet30.csv"
data30 = pd.read_csv(fileName)
fileName = fileDirectory + "\\finalDataSet60.csv"
data60 = pd.read_csv(fileName)
fileName = fileDirectory + "\\finalDataSet120.csv"
data120 = pd.read_csv(fileName)

# 2. Rozbicie danych na wejscie i wyjscie
X10,y10 = data10.iloc[:,:len(data10.values[0,:])-1].values, \
    data10.iloc[:,len(data10.values[0,:])-1].values
X30,y30 = data30.iloc[:,:len(data30.values[0,:])-1].values, \
    data30.iloc[:,len(data30.values[0,:])-1].values
X60,y60 = data60.iloc[:,:len(data60.values[0,:])-1].values, \
    data60.iloc[:,len(data60.values[0,:])-1].values
X120,y120 = data120.iloc[:,:len(data120.values[0,:])-1].values, \
    data120.iloc[:,len(data120.values[0,:])-1].values

# 3. Utworzenie zbiorow uczacych oraz walidacyjnych w proporcji 70/30
X10_uczacy,X10_walidacyjny,y10_uczacy,y10_walidacyjny = \
    train_test_split(X10,y10, test_size=0.3, random_state=0)
X30_uczacy,X30_walidacyjny,y30_uczacy,y30_walidacyjny = \
    train_test_split(X30,y30, test_size=0.3, random_state=0)
X60_uczacy,X60_walidacyjny,y60_uczacy,y60_walidacyjny = \
    train_test_split(X60,y60, test_size=0.3, random_state=0)
X120_uczacy,X120_walidacyjny,y120_uczacy,y120_walidacyjny = \
    train_test_split(X120,y120, test_size=0.3, random_state=0)

# 4. Optymalizacja modeli 
# 4.1 Otwarcie pliku wynikowego
fileName = fileDirectory + "\\wynik.csv"
wynik = open(fileName, "w")

# 4.1.2 Opisanie kolumn tabeli wynikowej
wynik.write("Historia,Dane wejsciowe, Model, C, gamma, score\n")

# 4.2 Utworzenie wektorow nazw
historia = [10, 30, 60, 120]
daneWejsciowe = ["Wszystkie,", "Predkosc,", "Przeciazenie,"]
model = ["Regresja logistyczna,", "SVM liniowy,", "SVM RBF,"]
score = 0;
score_min = 0.8

X_uczacy = []
X_walidacyjny = []
y_uczacy = []
y_walidacyjny = []

complete = 0
total = 4*3*2*4+4*3*4*4
per = 0
# 4.3 Petle uczace
for hist in range(len(historia)):
    #X_uczacy.clear()
    #X_walidacyjny.clear()
    #y_uczacy.clear()
    #y_walidacyjny.clear()

    if hist == 0:
        X_uczacy = X10_uczacy
        X_walidacyjny = X10_walidacyjny
        y_uczacy = y10_uczacy
        y_walidacyjny = y10_walidacyjny
    elif hist == 1:
        X_uczacy = X30_uczacy
        X_walidacyjny = X30_walidacyjny
        y_uczacy = y30_uczacy
        y_walidacyjny = y30_walidacyjny
    elif hist == 2:
        X_uczacy = X60_uczacy
        X_walidacyjny = X60_walidacyjny
        y_uczacy = y60_uczacy
        y_walidacyjny = y60_walidacyjny
    else:
        X_uczacy = X120_uczacy
        X_walidacyjny = X120_walidacyjny
        y_uczacy = y120_uczacy
        y_walidacyjny = y120_walidacyjny


    for c in range(-1,3):
        for mod in range(len(model)):
            if mod == 0:
                g = -100
                klasyfikator = LogisticRegression(penalty='l1', C=10**c,
                                                 random_state=0)
            elif mod == 1:
                g = -100
                klasyfikator = SVC(kernel='linear', C=10**c, 
                                   random_state=0)
            else:
                for g in range(-1,3):
                    klasyfikator = SVC(kernel='rbf', random_state=0, 
                                       gamma=10**g,C=10**c)

                    for dan in range(len(daneWejsciowe)):

                        if dan == 0:
                            klasyfikator.fit(X_uczacy,y_uczacy)
                            score = klasyfikator.score(X_walidacyjny,y_walidacyjny)
                        elif dan == 1:
                            klasyfikator.fit(X_uczacy[:historia[hist]],
                                             y_uczacy[:historia[hist]])
                            score = klasyfikator.score(X_walidacyjny[:historia[hist]],
                                                       y_walidacyjny[:historia[hist]])
                        else:
                            klasyfikator.fit(X_uczacy[historia[hist]:],
                                             y_uczacy[historia[hist]:])
                            score = klasyfikator.score(X_walidacyjny[historia[hist]:],
                                                       y_walidacyjny[historia[hist]:])

                    
                        if score > score_min:
                            wynik.write(str(historia[hist]) + ',')
                            wynik.write(daneWejsciowe[dan])
                            wynik.write(model[mod])
                            wynik.write(str(c) + ',')
                            wynik.write(str(g) + ',')
                            wynik.write(str(score) + '\n')

                        complete += 1
                        cur = complete/total
                        if cur > per+0.01:
                            per += 0.01
                            print(per)
                continue
                   

            for dan in range(len(daneWejsciowe)):
                if dan == 0:
                    klasyfikator.fit(X_uczacy,y_uczacy)
                    score = klasyfikator.score(X_walidacyjny,y_walidacyjny)
                elif dan == 1:
                    klasyfikator.fit(X_uczacy[:historia[hist]],y_uczacy[:historia[hist]])
                    score = klasyfikator.score(X_walidacyjny[:historia[hist]],
                                               y_walidacyjny[:historia[hist]])
                else:
                    klasyfikator.fit(X_uczacy[historia[hist]:],y_uczacy[historia[hist]:])
                    score = klasyfikator.score(X_walidacyjny[historia[hist]:],
                                               y_walidacyjny[historia[hist]:])
                
                if score > score_min:
                    wynik.write(str(historia[hist]) + ',')
                    wynik.write(daneWejsciowe[dan])
                    wynik.write(model[mod])
                    wynik.write(str(c) + ',')
                    wynik.write(str(g) + ',')
                    wynik.write(str(score) + '\n')

                            
                complete += 1
                cur = complete/total
                if cur > per+0.01:
                    per += 0.01
                    print(per)

wynik.close()