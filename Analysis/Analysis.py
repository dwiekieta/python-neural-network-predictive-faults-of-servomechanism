# Analysis
# Skrypt zawiera instrukcje przeksztalcajace dane zmagazynowane w plikach CSV 
#   do formy wejsciowej dla sieci neuronowej:
#       wektor predkosci rzeczywistej zostanie znormlaizowany poprzez odniesienie 
#       jego wartosci do wartosci oczekiwanej oraz wyrazenie roznicy predkosci
#       jako procent z wartosci oczekiwanej,
#
# Plik wyjsciowy bedzie plikiem zawierajacym kolumny:
#       predkoscNormalna przeciazenieNormalne rodzajRuchu

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

# 1. Znalezienie pliku z danymi ze wszsytkich testow
fileList = []
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
for name in os.listdir(fileDirectory):
    if name == "wszystkie.csv":
        fileList.append(name)
                  
# 2. Odczytanie danych z pliku
#fileName = fileDirectory + "\\" + name
fileName = fileDirectory + "\\RealDataSet.csv"
data = pd.read_csv(fileName)

# 3. Pobranie wektora etykiet
etykiety = data.values[:,4]

# 4. Normalizacja wektora predkosci rzeczywistej
# 4.1 Przetworzenie calego pliku danych 
predkoscNormalnaRaw = []
for i in range(len(data.values[:,1])):
    predkoscOczekiwana = data.values[i,1]
    predkoscRzeczywista = data.values[i,2]

    if predkoscRzeczywista > 0:
        predkoscNormalnaRaw.append(predkoscRzeczywista - predkoscOczekiwana);
    else:
        predkoscNormalnaRaw.append(predkoscRzeczywista + predkoscOczekiwana);
    
 # 4.2 Rozdzielenie danych na zestawy z automatycznym oraz manualnym strojeniem
 # 4.2.1 Wektory danych surowych
predkoscNormalnaRawP = []
predkoscNormalnaRawN = []
for i in range(len(predkoscNormalnaRaw)):
    if bool(etykiety[i]):
        predkoscNormalnaRawP.append(predkoscNormalnaRaw[i])
    else:
        predkoscNormalnaRawN.append(predkoscNormalnaRaw[i])

# 4.2.2 Relacyjne kroki czasowe
xP = [i for i in range(len(predkoscNormalnaRawP))]
xN = [i for i in range(len(predkoscNormalnaRawN))]

# 4.3 Reprezentacja danych
plt.scatter(xP,predkoscNormalnaRawP,color='blue',marker='x', label='autoTune')
plt.scatter(xN,predkoscNormalnaRawN,color='red',marker='o', label='manualTune')
plt.xlabel("Stempel czasowy, ms")
plt.ylabel("Roznica predkosci, pulsy")
plt.legend(loc='upper left')
plt.show()

# 5. Standaryzacja wektora predkosci
predkoscStandardowa = []
for i in range(len(predkoscNormalnaRaw)):
    if data.values[i,1] == 0:
        predkoscStandardowa.append(predkoscNormalnaRaw[i]/99999999)
    else:
        predkoscStandardowa.append(predkoscNormalnaRaw[i]/data.values[i,1])

    if predkoscStandardowa[i] > 1:
        predkoscStandardowa[i] = 1
    elif predkoscStandardowa[i] < -1:
        predkoscStandardowa[i] = -1

# 5.2 Rozdzielenie danych na zestawy z automatycznym oraz manualnym strojeniem
# 5.2.1 Wektory danych znormalizowanych
predkoscStandardowaP = []
predkoscStandardowaN = []
for i in range(len(predkoscStandardowa)):
    if bool(etykiety[i]):
        predkoscStandardowaP.append(predkoscStandardowa[i])
    else:
        predkoscStandardowaN.append(predkoscStandardowa[i])

# 5.2.2 Relacyjne kroki czasowe
xP = [i for i in range(len(predkoscStandardowaP))]
xN = [i for i in range(len(predkoscStandardowaN))]

# 5.3 Reprezentacja danych
plt.scatter(xP,predkoscStandardowaP,color='blue',marker='x', label='autoTune')
plt.scatter(xN,predkoscStandardowaN,color='red',marker='o', label='manualTune')
plt.xlabel("Stempel czasowy, ms")
plt.ylabel("Roznica predkosci")
plt.legend(loc='upper left')
plt.show()

# 6. Pobranie wektora przeciazenia
przeciarznieRaw = data.values[:,3]*0.001

# 6.2 Rozdzielenie wektora przeciazen na auto i manual
przeciarznieRawP = []
przeciarznieRawN = []
for i in range(len(przeciarznieRaw)):
    if bool(etykiety[i]):
        przeciarznieRawP.append(przeciarznieRaw[i])
    else:
        przeciarznieRawN.append(przeciarznieRaw[i])

# 6.2.2 Relacyjne kroki czasowe
xP = [i for i in range(len(przeciarznieRawP))]
xN = [i for i in range(len(przeciarznieRawN))]

# 6.3 Reprezentacja danych
plt.scatter(xP,przeciarznieRawP,color='blue',marker='x', label='autoTune')
plt.scatter(xN,przeciarznieRawN,color='red',marker='o', label='manualTune')
plt.xlabel("Stempel czasowy, ms")
plt.ylabel("Przeciazenie")
plt.legend(loc='upper left')
plt.show()

# 7. Normalizacja wektorow wejsciowych
predkoscNormalna = [((predkoscStandardowa[i]+1)/2) \
   for i in range(len(predkoscStandardowa))]

przeciazenieMax = max(przeciarznieRaw)
przeciazenieMin = min(przeciarznieRaw)
przeciarznieNormalne = [((przeciarznieRaw[i]-przeciazenieMin)/
                         (przeciazenieMax-przeciazenieMin)) \
                            for i in range(len(przeciarznieRaw))]

plt.scatter([i for i in range(len(przeciarznieNormalne))],
            przeciarznieNormalne,color='blue',marker='x', label='autoTune')  
plt.show()

# 8. Stworzenie pliku z danymi wejsciowymi do sieci neuronowej
#file = open(fileDirectory + "//dataSet.csv", "w")
file = open(fileDirectory + "//NEWdataSet.csv", "w")

file.write("predkoscNormalna,przeciazenieNormalne,rodzajRuchu\n")

for i in range(len(predkoscNormalna)):
    file.write(str(float(predkoscNormalna[i])) + ',' + 
               str(float(przeciarznieNormalne[i])) + ',' + str(etykiety[i]) + '\n')
file.close()