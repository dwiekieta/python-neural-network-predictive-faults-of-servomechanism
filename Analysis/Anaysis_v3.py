import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

# 1. Odczytanie danych z pliku
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
fileName = fileDirectory + "\\RealDataSet.csv"
data = pd.read_csv(fileName) 

# 2. Przesuniecie wykresu predkosci rzecz.
# 2.1 znalezienie minimum w v_oczekiwanej
min = 9999999
min_v_ocz = -1
for i in range(250):
    abs_val = abs(data.values[i,1])
    if (abs_val != 0) and (abs_val < min):
        min_v_ocz = i
        min = data.values[i,1]
# 2.2 znalezienie minimum w v_rzeczywistej
min = 9999999
min_v_rz = -1
for i in range(250):
    abs_val = abs(data.values[i,2])
    if (abs_val != 0) and (abs_val < min):
        min_v_rz = i
        min = data.values[i,2]
# 2.3 przesuniecie wektora predkosci oczekiwanej
print(min_v_ocz,min_v_rz)
przesuniecie = 0 #int((min_v_ocz - min_v_rz)/2)
p_set= [v for v in data.values[przesuniecie:,1]]
for j in range(przesuniecie):
    p_set.append(0)

# 2. Redukcja wektora wejsciowego
p_rel = abs(data.values[:,2]) - data.values[:,1];

for p in range(len(p_rel)):
    if data.values[p,1] != 0:
        dzielnik = data.values[p,1];
    
    p_rel[p] = p_rel[p]/dzielnik;

p_rel_arr = np.array(p_rel)
p_t = [i for i in range(len(p_rel))]

plt.rcParams.update({'font.size': 22})
'''
#plt.scatter(p_t,data.values[:,1]/100,color='blue',marker='x', label='prędkość oczekiwana')#'set speed')
#plt.scatter(p_t,data.values[:,2]/1000,color='red',marker='o', label='prędkość rzeczywista')#'real speed')
#plt.scatter(p_t,p_rel,color='green',marker='s', label='relative speed')
#plt.scatter(p_t,(abs(data.values[:,2])-data.values[:,1])/1000,color='red',marker='o', label='prędkość relatywna')#'moduł prędkości rzeczywistej')
plt.scatter(p_t,data.values[:,3],color='green',marker='s', label='moment obr.')
plt.xlabel("Stempel czasowy, ms")
plt.ylabel("Prędkość obrotowa wału, obr/min")#("Moment obr., *0.1%; Prędkość obr., *10obr/min")
plt.legend(loc='upper right')
plt.grid()
plt.show()
'''

# 3. Normalizacja i standaryzacja wektora predkosci

mms = MinMaxScaler()

stdc = StandardScaler()
p_norm = mms.fit_transform(p_rel_arr.reshape(-1, 1))
p_std = stdc.fit_transform(p_rel_arr.reshape(-1, 1))


plt.scatter(p_t,p_norm,color='blue',marker='x', label='prędkość znormalizowana')
#plt.scatter(p_t,p_std,color='c',marker='^', label='standardized relative speed') 
'''  
plt.xlabel("Stempel czasowy, ms")
plt.ylabel("Prędkość obrotowa wału, -")
plt.legend(loc='upper right')
plt.grid()
plt.show()
'''

# 4. Normalizacja i standaryzacja wektora przeciazenia 
t_norm = mms.fit_transform(data.values[:,3].reshape(-1, 1))
#t_std = stdc.fit_transform(data.values[:,3].reshape(-1, 1))

plt.scatter(p_t,t_norm,color='green',marker='s', label='znormalizowany moment obr.')
#plt.scatter(p_t,t_std,color='m',marker='.', label='standardized torque') 
plt.xlabel("Stempel czasowy, ms")
plt.ylabel("Moment obr., -; Prędkość obrotowa wału, -")
plt.legend(loc='upper right')
plt.grid()
plt.show()
'''
# 5. Stworzenie pliku z danymi wejsciowymi do sieci neuronowej
file = open(fileDirectory + "//STDdataSet.csv", "w")

file.write("predkoscNormalna,przeciazenieNormalne,rodzajRuchu\n")

for i in range(len(p_t)):
    file.write(str(float(p_std[i])) + ',' + 
               str(float(t_std[i])) + ',' + str(data.values[i,4]) + '\n')
file.close()
'''
