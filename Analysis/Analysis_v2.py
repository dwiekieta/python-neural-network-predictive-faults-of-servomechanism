import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

# 1. Odczytanie danych z pliku
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
fileName = fileDirectory + "\\RealDataSet.csv"
data = pd.read_csv(fileName) 

# 2. Redukcja wektora wejsciowego
p_rel = abs(data.values[:,2]) - data.values[:,1]
p_rel_arr = np.array(p_rel)
p_t = [i for i in range(len(p_rel))]

#plt.scatter(p_t,data.values[:,1],color='blue',marker='x', label='set speed')
#plt.scatter(p_t,abs(data.values[:,2]),color='red',marker='o', label='real speed')
plt.scatter(p_t,p_rel,color='green',marker='s', label='relative speed')
plt.xlabel("Time stempel, ms")
plt.ylabel("Speed, puls/s")
plt.legend(loc='upper right')
plt.show()

# 3. Normalizacja i standaryzacja wektora predkosci
mms = MinMaxScaler()
stdc = StandardScaler()
p_norm = mms.fit_transform(p_rel_arr.reshape(-1, 1))
p_std = stdc.fit_transform(p_rel_arr.reshape(-1, 1))

plt.scatter(p_t,p_norm,color='blue',marker='x', label='autoTune')
#plt.scatter(p_t,p_std,color='c',marker='^', label='standardized relative speed')   
plt.xlabel("Time stempel, ms")
plt.ylabel("Speed, puls/s")
plt.legend(loc='upper right')
plt.show()

# 4. Normalizacja i standaryzacja wektora przeciazenia 
t_norm = mms.fit_transform(data.values[:,3].reshape(-1, 1))
t_std = stdc.fit_transform(data.values[:,3].reshape(-1, 1))

#plt.scatter(p_t,data.values[:,3]*0.0047,color='blue',marker='x', label='autoTune')
plt.scatter(p_t,t_std,color='m',marker='.', label='standardized torque') 
plt.xlabel("Time stempel, ms")
plt.ylabel("Torque, -")
plt.legend(loc='upper right')
plt.show()

# 5. Stworzenie pliku z danymi wejsciowymi do sieci neuronowej
file = open(fileDirectory + "//STDdataSet.csv", "w")

file.write("predkoscNormalna,przeciazenieNormalne,rodzajRuchu\n")

for i in range(len(p_t)):
    file.write(str(float(p_std[i])) + ',' + 
               str(float(t_std[i])) + ',' + str(data.values[i,4]) + '\n')
file.close()