# Postprocessing
# Skrypt tworzacy plik wynikowy posiadajacy w wierszu historie
#   n probek przebiegu czasowego oraz etykiete rodzaju pracy


import os
import pandas as pd
import numpy as np    

# 1. Wczytanie bazy probek dataSet
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
#fileName = fileDirectory + "\\dataSet.csv"
fileName = fileDirectory + "\\STDdataSet.csv"
data = pd.read_csv(fileName)

# 2. Rozbicie danych na wejscie i wyjscie
y = data.iloc[:,2].values

# 3. Utworzenie macierzy z n iloscia probek historii
n = 100
X = np.ones(((len(y[:])-2*n),2*n+1))
k = 0

complete = 0
for i in range(len(y[:])-n-2):
    if y[i+n-1] != y[i]:
        k += 1
        continue
    for j in range(i,i+n):
        X[i-k,j-i] = data.values[j,0]
        X[i-k,j-i+n] = data.values[j,1]
    X[i-k,2*n] = y[i]

    com = i/(len(y[:])-n-2)
    if com > complete+0.005 :
        complete += 0.005
        print(complete)
        

# 4. Zapis pliku wynikowego
#file = open(fileDirectory + "//finalDataSet.csv", "w")
file = open(fileDirectory + "//STD100finalDataSet.csv", "w")

# 4.1 Wygenerowanie nazw kolumn
labels = [("predkosc" + str(i) + ',') for i in range(n)]
for i in range(n):
    labels.append("przeciazenie" + str(i) + ',')
labels.append("etykieta\n")

for i in range(2*n):
    file.write(labels[i])
file.write(labels[2*n])

# 4.2 Wpisanie danych do pliku csv
for i in range(len(y[:])-2*n-1):
    for j in range(2*n):
        file.write(str(X[i,j]) + ',')
    file.write(str(X[i,2*n]) + '\n')

file.close()


