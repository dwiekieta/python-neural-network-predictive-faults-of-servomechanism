# Preprocessing
# Skrypt zawiera instrukcje konwertujace pliki zebrane w fazie testowania 
#   serwomechanizmu, w formacie *.TXT oraz zwierajace dane w postaci 
#   wierszy, oddzielone spacjami w formacie: 
#       rrrr/mm/dd hh:mm:ss.SSS predkoscZadana predkoscRzeczywista przeciazenie 
#   Kazda nazwa pliku zawiera zakodowane etykiety: 
#       rodzajRuchu_predkoscZadana_obciazenie   
#
# Plikiem wynikowym dzialania skryptu jest plik z warosciamai oddzilonymi
#   przecinkami o rozszerzeniu CSV. Jego struktura zawiera kolumny w postaci
#       stempelCzasowy predkoscOczekiwana predkoscRzeczywista przeciazenie rodzajRuchu 
#   Nazwa pliku jest tzsama z nazwa pliku wejsciowego
#
# Dodatkowo zostana utworzone trzy pliki 
#   auto.csv 
#   manual.csv
#   wszystkie.csv

import os
import datetime
import time

# 1. Znalezienie plikow z danymi
fileList = []
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
for name in os.listdir(fileDirectory):
    if name.endswith(".TXT"):
        fileList.append(name)

# 2. Przetworzenie danych w plikach
for name in fileList:
    # (1) Otwarcie pliku z danymi
    fileName = fileDirectory + "\\" + name
    fileTXT = open(fileName,"r")

    # (2) Otwarcie pliku CSV
    # (2.1) Zmiana nazwy pliku wejsciowego na rozszerzenie *.csv
    nameCSV = list(name);
    nameCSV[-3] = 'c'
    nameCSV[-2] = 's'
    nameCSV[-1] = 'v'

    # (2.2) Otwarcie pliku CSV
    # (2.2.1) Otwarcie pliku
    fileCSName = fileDirectory + "\\" + "".join(nameCSV)
    fileCSV = open(fileCSName, "w")

    # (2.2.2) Nadanie nazw kolumn
    fileCSV.write("stempelCzasowy,predkoscOczekiwana," +
                  "predkoscRzeczywista,przeciazenie,rodzajRuchu\n")

    # (3) Klasyfikacja danych w pliku
    if name[0] == 'p':
       etykieta = 1
    else:
       etykieta = 0

    # (4) Przeczytanie znakow na poczatku pliku
    fileTXT.read(2)

    # (5) Odczytanie kazdej linii danych w pliku
    czas0 = 0
    for line in fileTXT:
        # (5.1) Pomijanie linii bez danych
        # (5.1.2) Ostatnia linia
        if line[-1] == '\x00':
            break

        # (5.1.1) Pusta lina
        if  line[1] == '\n':
            continue
        


        # Bufory zmiennych zapisanych w kodzie ASCII
        rokStr = []
        miesiacStr = []
        dzienStr = []
        godzinaStr = []
        minutaStr = []
        sekundaStr = []
        milisekundaStr = []
        predkoscOczekiwanaPulsyStr = []
        predkoscRzeczywistaPulsyStr = []
        przeciazenieStr = []
        dcnt = 0;       # licznik danych
        c0 = ' ';       # ostatni znak
        
        # (5.2) Interpretacja danych w pobranej linii
        for c in line:
            # (5.2.1) Pominiecie bialych znakow oznaczajacych przejscie do nowej zmiennej
            if (c == '\x00' and c0 == '\x00') or c == '\t':
                c0 = c
                dcnt += 1
                continue

            # (5.2.2) Pominiecie znakow oznaczjacych przejscie do kolejnej zmiennej
            if c == ' ' or c == '/' or c == '\n':
                c0 = c
                dcnt += 1
                continue

            # (5.2.3) Pominiecie separatorow miedzy danymi
            if c == ':' or c == '.':
                c0 = c
                dcnt += 1
                continue

            # (5.2.4) Pominiecie znakow NULL
            if c == '\x00':
                c0 = c
                continue

            # (5.2.5) Przypisanie danych do listy zmiennych
            if dcnt == 0:
                rokStr.append(c)
            elif dcnt == 1:
                miesiacStr.append(c)
            elif dcnt == 2:
                dzienStr.append(c)
            elif dcnt == 3:
                godzinaStr.append(c)
            elif dcnt == 4:
                minutaStr.append(c)
            elif dcnt == 5:
                sekundaStr.append(c)
            elif dcnt == 6:
                milisekundaStr.append(c)
            elif dcnt == 7:
                predkoscOczekiwanaPulsyStr.append(c)
            elif dcnt == 8:
                predkoscRzeczywistaPulsyStr.append(c)
            else:
                przeciazenieStr.append(c)

            c0 = c

        # (5.3) Zrzutowanie zmiennych w formacie ASCII na typy liczbowe
        rok = int(''.join(rokStr))
        miesiac = int(''.join(miesiacStr))
        dzien = int(''.join(dzienStr))
        godzina = int(''.join(godzinaStr))
        minuta = int(''.join(minutaStr))
        sekunda = int(''.join(sekundaStr))
        milisekunda = int(''.join(milisekundaStr))
        predkoscOczekiwanaPulsy = int(''.join(predkoscOczekiwanaPulsyStr))
        predkoscRzeczywistaPulsy = int(''.join(predkoscRzeczywistaPulsyStr))
        przeciazenie = int(''.join(przeciazenieStr))

        # (5.4) Ujednolicenie stempla czasowego na liczbe milisekund
        # (5.4.1) Konwersja czasu do milisekund
        totalMs = int(datetime.timedelta(dzien,sekunda,0,milisekunda,
                                         minuta,godzina,0).total_seconds() * 1000)

        # (5.4.2) Normalizacja czasu
        if czas0 == 0:
            czas0 = totalMs
            totalMs = 0
        else:
             totalMs -= czas0

        # (5.5) Wpisanie linii danych do pliku CSV
        fileCSV.write(str(totalMs) + ',' + str(predkoscOczekiwanaPulsy) + ',' +
                     str(predkoscRzeczywistaPulsy) + ',' + 
                     str(przeciazenie) + ',' + 
                     str(etykieta) + '\n')

    fileTXT.close
    fileCSV.close
    
# 6. Utworzenie plikow calosciowych
# 6.1 Wyszukanie plikow z rozszerzeniem csv
fileList.clear()
fileDirectory = "E:\PWr\AiR\VI\Sieci neuronowe\Dataset"
for name in os.listdir(fileDirectory):
    if (name[0] == 'n' or name[0] == 'p') and name.endswith(".csv"):
        fileList.append(name)

# 6.2 Otwarcie plikow wyjsciowych
fileName =  fileDirectory + "\\auto.csv"
fileAuto = open(fileName,"w")
fileName =  fileDirectory + "\\manual.csv"
fileManual = open(fileName,"w")
fileName =  fileDirectory + "\\wszystkie.csv"
fileWszystkie = open(fileName,"w")

# (2.2.2) Nadanie nazw kolumn
fileAuto.write("stempelCzasowy,predkoscOczekiwana," +
                  "predkoscRzeczywista,przeciazenie,rodzajRuchu\n")
fileManual.write("stempelCzasowy,predkoscOczekiwana," +
                  "predkoscRzeczywista,przeciazenie,rodzajRuchu\n")
fileWszystkie.write("stempelCzasowy,predkoscOczekiwana," +
                  "predkoscRzeczywista,przeciazenie,rodzajRuchu\n")

# 6.2 Odczytnie danych
for name in fileList:
    # (1) Otwarcie pliku z danymi
    fileName = fileDirectory + "\\" + name
    fileCSV = open(fileName,"r")

    # (2) Sprawdzenie rodzaju danych w pliku
    if name[0] == 'p':
        auto = True
    else:
        auto = False

    fileCSV.readline();

    # (3) Wpisanie wartosci do plikow
    for line in fileCSV:
        fileWszystkie.writelines(line)

        if auto:
            fileAuto.writelines(line)
        else:
            fileManual.writelines(line)

    fileCSV.close()

fileAuto.close()
fileManual.close()
fileWszystkie.close()


