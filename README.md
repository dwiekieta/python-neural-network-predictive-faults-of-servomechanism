In this solution are scripts referended to my diploma work "Predictive fault detection in
	servomechanism by neural networks" at WUST faculty of Electronics, field of study
	Control Engineering and Robotics

	Preprocessing:
		- reading TXT data(from PLC data file)
		- separating meaningfull data
		- add flag about type of work
		- save data to CSV file
	Analysis: 
		- reading data from CSV
		- reducing data
		- normalizing data
		- save data to CSV file
	Postprocessing:
		- reading data from CSV file
		- making matrixes of data to recognize by neural network 
		- save matrixes to CSV file
	NeuralNetwork:
		- reading data from CSV file
		- crating predictors 
			- logistic regresion
			- SVC
			- RBF
		- chceck quality of predictions
		- save results
